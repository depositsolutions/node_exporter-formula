# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "node_exporter/map.jinja" import node_exporter with context %}

node_exporter-remove-service:
  service.dead:
    - name: node_exporter
  file.absent:
    - name: /etc/systemd/system/node_exporter.service
    - require:
      - service: node_exporter-remove-service
  cmd.run:
    - name: 'systemctl daemon-reload'
    - onchanges:
      - file: node_exporter-remove-service

node_exporter-remove-symlink:
   file.absent:
    - name: {{ node_exporter.bin_dir}}/node_exporter
    - require:
      - node_exporter-remove-service

node_exporter-remove-binary:
   file.absent:
    - name: {{ node_exporter.dist_dir}}
    - require:
      - node_exporter-remove-symlink

node_exporter-remove-config:
    file.absent:
      - name: {{ node_exporter.config_dir }}
      - require:
        - node_exporter-remove-binary
