#node_exporter-formula

A saltstack formula created to setup [node_exporter monitoring server](https://github.com/prometheus/node_exporter.


# Available states

## init

The essential node_exporter state running both ``install``.

## install

- Download node_exporter release from Github into the dist dir  (defaults to /opt/node_exporter/dist).
- link the binary (defaults to /usr/bin/node_exporter).
- Register the appropriate service definition with systemd.
- The NTP collector is switched on/off depending in the grain['virtual']. If this grain is not "LXC" it will be installed automatically.
  
This state can be called independently.

## uninstall

Remove the service, binaries, and configuration files. The data itself will be kept and needs to be removed manually, just to be on the safe side.

This state must always be called independently.

# Testing

## Prerequisites
The tests are using test kitchen with [testinfra](http://testinfra.readthedocs.io/en/latest/) as verifier, so you need to have
- Ruby installed
- Python installed
- Vagrant installed

If you want to add tests you might want to take a look at the [documentation](http://testinfra.readthedocs.io/en/latest/modules.html#) for the modules.

## Running the test
If you run the tests for the first time you might need to run ``bundle install`` and ``pip install -r requirements.txt`` first. Afterwards you can run ``kitchen test --concurrency 12 --parallel``.  
Be aware that ``--concurrency 12 --parallel`` might be a bit too demanding for some machines. You can either ommit these parameters or adjust the to fit your machines capacity.
